# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-05-08 15:19+0000\n"
"PO-Revision-Date: 2024-05-08 16:55+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Help Tails: Mac and PC UEFI hardware needed\"]]\n"
msgstr "[[!meta title=\"Ajudeu a Tails: es necessita maquinari per a Mac i PC UEFI\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Sat, 12 Oct 2013 09:00:00 +0000\"]]\n"
msgstr "[[!meta date=\"Sat, 12 Oct 2013 09:00:00 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"For the moment, Tails has very limited support for Mac.  Also, Tails lacks "
"UEFI support, so quite a few recent PC's refuse to boot from a Tails USB "
"stick."
msgstr ""
"De moment, Tails té un suport molt limitat per a Mac. A més, Tails no té "
"suport UEFI, de manera que uns quants ordinadors recents es neguen a "
"arrencar des d'un llapis USB de Tails."

#. type: Plain text
msgid ""
"We will be working on this in the next few months. However, our testing and "
"development possibilities are limited: we do not own the right hardware yet."
msgstr ""
"Treballarem en això durant els propers mesos. Tanmateix, les nostres "
"possibilitats de prova i desenvolupament són limitades: encara no tenim el "
"maquinari adequat."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Title =
#, no-wrap
msgid "What we need"
msgstr "El que necessitem"

#. type: Plain text
msgid ""
"If you can afford it, please consider donating known-working hardware of the "
"kind described below."
msgstr ""
"Si us ho podeu permetre, considereu la possibilitat de donar un maquinari "
"que funcioni conegut del tipus que es descriu a continuació."

#. type: Plain text
msgid ""
"Alternatively, you may want to consider [[donating through Bitcoin|donate]] "
"and dropping us an email to earmark your donation for hardware purchase."
msgstr ""
"Alternativament, potser voldreu considerar [[donar mitjançant Bitcoin|"
"donate]] i enviar-nos un correu electrònic per destinar la vostra donació a "
"la compra de maquinari."

#. type: Plain text
msgid ""
"Else, there are [[many other ways|contribute]] you can contribute to Tails :)"
msgstr ""
"En cas contrari, hi ha [[moltes altres maneres|contribute]] amb les quals "
"podeu contribuir a Tails"

#. type: Title -
#, no-wrap
msgid "MacBook Pro or MacBook Air (2010 or later)"
msgstr "MacBook Pro o MacBook Air (2010 o posterior)"

#. type: Bullet: '* '
msgid ""
"EFI 2.6 firmware or later: you can check this on the [Apple firmware updates "
"page](https://support.apple.com/kb/HT1237)"
msgstr ""
"Microprogramari EFI 2.6 o posterior: podeu comprovar-ho a la [pàgina "
"d'actualitzacions del microprogramari d'Apple](https://support.apple.com/kb/"
"HT1237)"

#. type: Bullet: '* '
msgid "preferably with embedded DVD reader"
msgstr "preferiblement amb lector de DVD incrustat"

#. type: Bullet: '* '
msgid "preferably 13\""
msgstr "preferiblement 13\""

#. type: Title -
#, no-wrap
msgid "Recent PC laptops"
msgstr "Ordinadors portàtils recents"

#. type: Bullet: '* '
msgid ""
"must support booting Debian off UEFI: send us the exact brand and model of "
"the hardware you could donate, and we will happily check this for you"
msgstr ""
"ha de ser compatible amb l'arrencada de Debian fora de UEFI: envieu-nos la "
"marca i el model exactes del maquinari que podeu donar, i ho comprovarem "
"encantats"

#. type: Bullet: '* '
msgid "well supported by Debian Wheezy"
msgstr "ben recolzat per Debian Wheezy"

#. type: Bullet: '* '
msgid "preferably 13\" or less"
msgstr "preferiblement 13\" o menys"

#. type: Title -
#, no-wrap
msgid "When and how many"
msgstr "Quan i quants"

#. type: Plain text
msgid ""
"We need this hardware by the end of November but the sooner, the better."
msgstr ""
"Necessitem aquest maquinari a finals de novembre, però com més aviat millor."

#. type: Plain text
msgid ""
"We could definitely make use of several such computers. One item of each "
"category is enough for initial development, but it would be much better not "
"to rely on a single person for maintaining support of this kind of hardware."
msgstr ""
"Definitivament podríem fer ús de diversos ordinadors d'aquest tipus. Un "
"element de cada categoria és suficient per al desenvolupament inicial, però "
"seria molt millor no confiar en una sola persona per mantenir el suport "
"d'aquest tipus de maquinari."

#. type: Title =
#, no-wrap
msgid "How to donate"
msgstr "Com fer una donació"

#. type: Plain text
msgid "Please send offers to <info@tails.net>."
msgstr "Envieu ofertes a <info@tails.net>."

#. type: Plain text
msgid "Thanks in advance!"
msgstr "Gràcies per endavant!"
