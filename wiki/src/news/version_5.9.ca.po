# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-04-05 02:50+0000\n"
"PO-Revision-Date: 2024-04-23 22:41+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 5.9 is out\"]]\n"
msgstr "[[!meta title=\"Tails 5.9\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 23 Jan 2023 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 23 Jan 2023 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid "We are sorry that Tails 5.8 affected many of you so badly."
msgstr "Lamentem que Tails 5.8 hagi afectat tant a molts de vosaltres."

#. type: Plain text
msgid ""
"Thanks to your patience and feedback, we were able to solve most of these "
"new issues."
msgstr ""
"Gràcies a la vostra paciència i comentaris, hem pogut resoldre la majoria "
"d'aquests nous problemes."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Canvis i actualitzacions</h1>\n"

#. type: Plain text
msgid ""
"- Update *Tor Browser* to [12.0.2](https://blog.torproject.org/new-release-"
"tor-browser-1202)."
msgstr ""
"- S'ha actualitzat el *Navegador Tor* a la versió [12.0.2](https://blog."
"torproject.org/new-release-tor-browser-1202)."

#. type: Plain text
msgid "- Update the *Tor* client to 0.4.7.13."
msgstr "- S'ha actualitzat el client de *Tor* a la versió 0.4.7.13."

#. type: Bullet: '- '
msgid ""
"Simplify the error screen of the *Tor Connection* assistant when connecting "
"automatically."
msgstr ""
"S'ha simplificat la pantalla d'error de l'assistent de *Connexió Tor* quan "
"es connecta automàticament."

#. type: Plain text
msgid "- Improve the wording of the backup utility for the Persistent Storage."
msgstr ""
"- S'ha millorat la redacció de la utilitat de còpia de seguretat per a "
"l'Emmagatzematge Persistent."

#. type: Plain text
msgid "- Remove the confirmation dialog when starting the *Unsafe Browser*."
msgstr ""
"- S'ha eliminat el diàleg de confirmació quan s'inicia el *Navegador "
"Insegur*."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Problemes solucionats</h1>\n"

#. type: Plain text
msgid "- Fix support for some graphics cards:"
msgstr "- S'ha corregit el suport per a algunes targetes gràfiques:"

#. type: Bullet: '  * '
msgid ""
"Update the *Linux* kernel to 6.0.12. This improves the support for newer "
"hardware in general: graphics, Wi-Fi, and so on. ([[!tails_ticket 18467]])"
msgstr ""
"S'ha actualitzat el nucli de *Linux* a la versió 6.0.12. Això millora la "
"compatibilitat amb el maquinari més nou en general: gràfics, Wi-Fi, etc. ([[!"
"tails_ticket 18467]])"

#. type: Bullet: '  * '
msgid ""
"Remove from the Troubleshooting Mode 2 boot options that break support for "
"some graphics cards: `nomodeset` and `vga=normal`. ([[!tails_ticket 19321]])"
msgstr ""
"S'ha eliminat l'opció d'arrencada del mode de resolució de problemes 2 que "
"trencava la compatibilitat amb algunes targetes gràfiques: `nomodeset` i "
"`vga=normal`. ([[!tails_ticket 19321]])"

#. type: Plain text
#, no-wrap
msgid ""
"  Please let us know if the support for your graphics cards has been fixed or\n"
"  is still broken.\n"
msgstr ""
"  Si us plau, feu-nos saber si s'ha solucionat el suport per a les vostres targetes gràfiques o\n"
"  encara està trencat.\n"

#. type: Bullet: '- '
msgid ""
"Fix starting AppImages that use the Qt toolkit like *Feather* and *Bitcoin-"
"Qt*. ([[!tails_ticket 19326]])"
msgstr ""
"S'ha corregit l'arrencada d'AppImages que utilitzen el conjunt d'eines Qt "
"com ara *Feather* i *Bitcoin-Qt*. ([[!tails_ticket 19326]])"

#. type: Plain text
msgid ""
"- Fix clipboard encryption and decryption in *Kleopatra*. ([[!tails_ticket "
"19329]])"
msgstr ""
"- S'ha arreglat l'encriptatge i el desencriptatge del porta-retalls a "
"*Kleopatra*. ([[!tails_ticket 19329]])"

#. type: Plain text
msgid "- Fix at least 2 cases of Persistent Storage not activating:"
msgstr ""
"- S'han arreglat almenys dos casos de fallada de l'activació de "
"l'Emmagatzematge Persistent:"

#. type: Bullet: '  * '
msgid "When activation takes longer ([[!tails_ticket 19347]])"
msgstr "Quan l'activació triga més ([[!tails_ticket 19347]])"

#. type: Bullet: '  * '
msgid ""
"When the Dotfiles feature includes symbolic links ([[!tails_ticket 19346]])"
msgstr ""
"Quan la funció Dotfiles inclou enllaços simbòlics ([[!tails_ticket 19346]])"

#. type: Plain text
#, no-wrap
msgid ""
"  Please keep reporting issues with the new Persistent Storage. We give them\n"
"  top priority!\n"
msgstr ""
"  Continueu informant de problemes amb el nou Emmagatzematge Persistent. Els donem\n"
"  prioritat màxima!\n"

#. type: Plain text
msgid "- Fix 3 clipboard operations with *KeePassXC*:"
msgstr "- S'han corregit tres operacions del porta-retalls amb *KeePassXC*:"

#. type: Bullet: '  * '
msgid "Copying a passphrase to unlock a database ([[!tails_ticket 19237]])"
msgstr ""
"Copiar una contrasenya per desbloquejar una base de dades ([[!tails_ticket "
"19237]])"

#. type: Bullet: '  * '
msgid "Using the auto-type feature ([[!tails_ticket 19339]])"
msgstr "Ús de la funció d'escriptura automàtica ([[!tails_ticket 19339]])"

#. type: Bullet: '  * '
msgid "Clearing passwords automatically from the clipboard after 10 seconds"
msgstr ""
"Esborrat automàtic de les contrasenyes del porta-retalls després de 10 segons"

#. type: Bullet: '- '
msgid ""
"Fix the display of the applications menu that was broken in some GTK3 "
"applications installed as Additional Software. ([[!tails_ticket 19371]])"
msgstr ""
"S'ha corregit la visualització del menú d'aplicacions que es va trencar en "
"algunes aplicacions GTK3 instal·lades com a Programari Addicional. ([[!"
"tails_ticket 19371]])"

#. type: Bullet: '- '
msgid ""
"Localize the homepage of *Tor Browser* when started from the *Tor "
"Connection* assistant. ([[!tails_ticket 19369]])"
msgstr ""
"S'ha adaptat la llengua de la pàgina d'inici del *Navegador Tor* quan "
"s'inicia des de l'assistent de *Connexió Tor*. ([[!tails_ticket 19369]])"

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."
msgstr ""
"Per a més detalls, llegiu el nostre [[!tails_gitweb debian/changelog desc="
"\"registre de canvis\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Problemes coneguts</h1>\n"

#. type: Plain text
msgid ""
"Please keep reporting issues with the new Persistent Storage and when "
"starting on graphics cards that used to work with Tails."
msgstr ""
"Si us plau, seguiu informant de problemes amb el nou Emmagatzematge "
"Persistent i quan inicieu amb targetes gràfiques que solien funcionar amb "
"Tails."

#. type: Title ##
#, no-wrap
msgid "*Tor Browser* has no minimize and maximize buttons ([[!tails_ticket 19328]])"
msgstr "El *Navegador Tor* no té botons de minimització i maximització ([[!tails_ticket 19328]])"

#. type: Plain text
msgid "To work around this:"
msgstr "Per solucionar això:"

#. type: Bullet: '1. '
msgid ""
"Right-click on the *Tor Browser* tab in the window list at the bottom of the "
"screen."
msgstr ""
"Feu clic amb el botó dret a la pestanya *Navegador Tor* a la llista de "
"finestres a la part inferior de la pantalla."

#. type: Bullet: '1. '
msgid "Choose **Minimize** or **Maximize**."
msgstr "Trieu **Minimitza** o **Maximitza**."

#. type: Title ##
#, no-wrap
msgid "Welcome Screen and Tor Connection don't fit on 800×600 ([[!tails_ticket 19324]])"
msgstr "La Pantalla de Benvinguda i la Connexió Tor no encaixen en 800×600 ([[!tails_ticket 19324]])"

#. type: Plain text
msgid ""
"The top of the Welcome Screen and some button of the Tor Connection "
"assistant are cut out on small displays (800×600), like virtual machines."
msgstr ""
"La part superior de la Pantalla de Benvinguda i algun botó de l'assistent de "
"Connexió Tor es tallen en pantalles petites (800×600), com les màquines "
"virtuals."

#. type: Plain text
msgid "You can press **Alt+S** to start Tails."
msgstr "Podeu prémer **Alt+S** per iniciar Tails."

#. type: Title ##
#, no-wrap
msgid "Progress bar of *Tor Connection* gets stuck around 50% ([[!tails_ticket 19173]])"
msgstr "La barra de progrés de *Connexió Tor* s'encalla al voltant del 50% ([[!tails_ticket 19173]])"

#. type: Plain text
msgid ""
"When using a custom Tor `obfs4` bridge, the progress bar of *Tor Connection* "
"sometimes gets stuck halfway through and becomes extremely slow."
msgstr ""
"Quan s'utilitza un pont de Tor `obfs4` personalitzat, la barra de progrés de "
"*Connexió Tor* de vegades s'encalla a la meitat i es torna extremadament "
"lenta."

#. type: Plain text
msgid "To fix this, you can either:"
msgstr "Per solucionar-ho, podeu:"

#. type: Bullet: '* '
msgid "Close and reopen *Tor Connection* to speed up the initial connection."
msgstr ""
"Tancar i tornar a obrir *Connexió Tor* per accelerar la connexió inicial."

#. type: Bullet: '* '
msgid "Try a different `obfs4` bridge."
msgstr "Provar un pont `obfs4` diferent."

#. type: Plain text
#, no-wrap
msgid ""
"  This issue only affects outdated obfs4 bridges and does not happen with\n"
"  obfs4 bridges that run version 0.0.12 or later.\n"
msgstr ""
"  Aquest problema només afecta els ponts obfs4 obsolets i no passa amb\n"
"  els ponts obfs4 que executen la versió 0.0.12 o posterior.\n"

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr ""
"Vegeu la llista de [[problemes de llarga durada|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.9</h1>\n"
msgstr "<h1 id=\"get\">Obtenir Tails 5.9</h1>\n"

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr "Per actualitzar el vostre llapis USB de Tails i mantenir el vostre Emmagatzematge Persistent"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.9."
msgstr ""
"- Les actualitzacions automàtiques estan disponibles des de Tails 5.0 o "
"posterior a la versió 5.9."

#. type: Plain text
#, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Podeu [[reduir la mida de la baixada|doc/upgrade#reduce]] de futures\n"
"  actualitzacions automàtiques fent una actualització manual a la darrera "
"versió.\n"

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Si no podeu fer una actualització automàtica o si Tails no s'inicia després "
"d'una actualització automàtica, proveu de fer una [[actualització manual|doc/"
"upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "Per instal·lar Tails en un nou llapis USB"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Seguiu les nostres instruccions d'instal·lació:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Instal·lar des de Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Instal·lar des de macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Instal·lar des de Linux|install/linux]]"

#. type: Bullet: '  - '
msgid ""
"[[Install from Debian or Ubuntu using the command line and GnuPG|install/"
"expert]]"
msgstr ""
"[[Instal·lar des de Debian o Ubuntu mitjançant la línia d'ordres i GnuPG|"
"install/expert]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>L'Emmagatzematge Persistent del llapis USB es "
"perdrà si\n"
"instal·leu en comptes d'actualitzar.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Per només baixar"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.9 directly:"
msgstr ""
"Si no necessiteu instruccions d'instal·lació o actualització, podeu baixar "
"Tails 5.9 directament:"

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Per a llapis USB (imatge USB)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Per a DVD i màquines virtuals (imatge ISO)|install/download-iso]]"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"next\">What's coming up?</h1>\n"
msgstr "<h1 id=\"next\">Què ve?</h1>\n"

#. type: Plain text
msgid "Tails 5.10 is [[scheduled|contribute/calendar]] for February 21."
msgstr "Tails 5.10 està [[programat|contribute/calendar]] per al 21 de febrer."
