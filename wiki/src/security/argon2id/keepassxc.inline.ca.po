# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-09-04 09:56+0200\n"
"PO-Revision-Date: 2024-02-09 14:37+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Bullet: '   1. '
msgid "Choose **Applications**&nbsp;▸ **KeePassXC**."
msgstr "Trieu **Aplicacions**&nbsp;▸ **KeePassXC**."

#. type: Bullet: '   1. '
msgid "Choose **Tools**&nbsp;▸ **Password Generator**."
msgstr "Trieu **Eines**&nbsp;▸ **Generador de contrasenyes**."

#. type: Bullet: '   1. '
msgid "Switch to the **Passphrase** tab."
msgstr "Canvieu a la pestanya **Contrasenya**."

#. type: Plain text
#, no-wrap
msgid "      A very strong passphrase of 7 random words is automatically generated.\n"
msgstr ""
"      Es genera automàticament una contrasenya molt forta de set paraules "
"aleatòries.\n"

#. type: Plain text
#, no-wrap
msgid "      <div class=\"caution\">\n"
msgstr "      <div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid "      <p>It is impossible to recover your passphrase if you forget it!</p>\n"
msgstr "      <p>És impossible recuperar la contrasenya si l'oblideu!</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"      <p>To help you remember your passphrase, you can write it on a piece of\n"
"      paper, store it in your wallet for a few days, and destroy it once\n"
"      you know it well.</p>\n"
msgstr ""
"      <p>Per ajudar-vos a recordar la vostra contrasenya, podeu escriure-la "
"en un tros de\n"
"      paper, guardar-la a la cartera durant uns dies i destruir-la una "
"vegada\n"
"      la sabeu bé.</p>\n"

#. type: Plain text
#, no-wrap
msgid "      </div>\n"
msgstr "      </div>\n"
