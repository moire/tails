# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:26+0100\n"
"PO-Revision-Date: 2023-08-01 15:22+0000\n"
"Last-Translator: drebs <drebs@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, fuzzy
#| msgid "[[Introduction to the Persistent Storage|persistent_storage]]"
msgid "- [[Introduction to the Persistent Storage|persistent_storage]]"
msgstr "[[Introdução ao Armazenamento Persistente|persistent_storage]]"

#. type: Bullet: '  * '
msgid "[[Creating the Persistent Storage|persistent_storage/create]]"
msgstr "[[Criando o Armazenamento Persistente|persistent_storage/create]]"

#. type: Bullet: '  * '
msgid "[[Configuring the Persistent Storage|persistent_storage/configure]]"
msgstr ""
"[[Configurando o Armazenamento Persistente|persistent_storage/configure]]"

#. type: Bullet: '  * '
msgid "[[Unlocking and using the Persistent Storage|persistent_storage/use]]"
msgstr ""
"[[Destravando e usando o Armazenamento Persistente|persistent_storage/use]]"

#. type: Bullet: '  * '
msgid ""
"[[Installing additional software|persistent_storage/additional_software]]"
msgstr ""
"[[Instalando programas adicionais|persistent_storage/additional_software]]"

#. type: Bullet: '  * '
msgid ""
"[[Making a backup of your Persistent Storage|persistent_storage/backup]]"
msgstr ""
"[[Fazendo um backup do seu Armazenamento Persistente|persistent_storage/"
"backup]]"

#. type: Bullet: '  * '
msgid "[[Deleting the Persistent Storage|persistent_storage/delete]]"
msgstr "[[Apagando o Armazenamento Persistente|persistent_storage/delete]]"

#. type: Plain text
#, fuzzy
#| msgid "Advanced topics on the Persistent Storage"
msgid "- Advanced topics on the Persistent Storage"
msgstr "Tópicos avançados sobre o Armazenamento Persistente"

#. type: Bullet: '  * '
msgid ""
"[[Changing the passphrase of the Persistent Storage|persistent_storage/"
"passphrase]]"
msgstr ""
"[[Mudando a senha do Armazenamento Persistente|persistent_storage/"
"passphrase]]"

#. type: Bullet: '  * '
msgid ""
"[[Opening your Persistent Storage from another operating system|"
"persistent_storage/open]]"
msgstr ""
"[[Abrindo seu Armazenamento Persistente em um outro sistema operacional|"
"persistent_storage/open]]"

#. type: Bullet: '  * '
#, fuzzy
#| msgid ""
#| "[[Rescuing the Persistent Storage of a broken Tails|persistent_storage/"
#| "rescue]]"
msgid ""
"[[Recover the Persistent Storage of a broken Tails|persistent_storage/"
"recover]]"
msgstr ""
"[[Recuperando o Armazenamento Persistente de um Tails quebrado|"
"persistent_storage/rescue]]"

#. type: Bullet: '  * '
msgid ""
"[[Checking the file system of the Persistent Storage|persistent_storage/"
"check]]"
msgstr ""
"[[Verificando o sistema de arquivos do Armazenamento Persistente|"
"persistent_storage/check]]"
