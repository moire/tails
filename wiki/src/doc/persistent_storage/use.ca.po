# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:26+0100\n"
"PO-Revision-Date: 2024-04-01 13:29+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Unlocking and using the Persistent Storage\"]]\n"
msgstr "[[!meta title=\"Desbloqueig i ús de l'Emmagatzematge Persistent\"]]\n"

#. type: Plain text
msgid ""
"To unlock your Persistent Storage and use the data that is stored in it:"
msgstr ""
"Per desbloquejar el vostre Emmagatzematge Persistent i utilitzar les dades "
"que hi ha emmagatzemades:"

#. type: Bullet: '1. '
msgid ""
"When starting Tails, in the **Persistent Storage** section of the [[Welcome "
"Screen|first_steps/welcome_screen]], enter your passphrase and click "
"**Unlock Encryption**."
msgstr ""
"Quan inicieu Tails, a la secció **Emmagatzematge Persistent** de la "
"[[Pantalla de Benvinguda|first_steps/welcome_screen]], introduïu la vostra "
"contrasenya i feu clic a **Desbloqueja l'encriptació**."

#. type: Plain text
#, no-wrap
msgid "   [[!img first_steps/welcome_screen/unlock_persistent_storage.png link=\"no\" class=\"screenshot\" alt=\"\"]]\n"
msgstr ""
"   [[!img first_steps/welcome_screen/unlock_persistent_storage.png link=\"no"
"\" class=\"screenshot\" alt=\"\"]]\n"

#. type: Bullet: '1. '
msgid "Start Tails."
msgstr "Inicieu Tails."

#. type: Bullet: '1. '
msgid ""
"The data corresponding to each feature of the Persistent Storage that is "
"turned on is automatically available. For example:"
msgstr ""
"Les dades corresponents a cada funció de l'Emmagatzematge Persistent que "
"està activada estan disponibles automàticament. Per exemple:"

#. type: Bullet: '   - '
msgid ""
"Your personal files in the *Persistent* folder are accessible from "
"**Places**&nbsp;▸ **Persistent**, if the [[Persistent Folder|"
"configure#persistent_folder]] feature is turned on."
msgstr ""
"Els vostres fitxers personals de la carpeta *Persistent* són accessibles des "
"de **Llocs**&nbsp;▸ **Persistent**, si la funció [[Carpeta Persistent|"
"configure#persistent_folder]] està activada."

#. type: Bullet: '   - '
msgid ""
"Your persistent bookmarks are available in *Tor Browser*, if the [[Tor "
"Browser Bookmarks|configure#bookmarks]] feature is turned on."
msgstr ""
"Les vostres adreces d'interès persistents estan disponibles al *Navegador "
"Tor*, si la funció [[Adreces d'Interès del Navegador Tor|"
"configure#bookmarks]] està activada."

#. type: Bullet: '   - '
msgid ""
"Your [[Additional Software|additional_software]] packages are automatically "
"installed when starting Tails, if the [[Additional Software|"
"configure#additional_software]] feature is turned on."
msgstr ""
"Els vostres paquets de [[Programari Addicional|additional_software]] "
"s'instal·len automàticament en iniciar Tails, si la funció [[Programari "
"Addicional|configure#additional_software]] està activada."

#. type: Plain text
#, no-wrap
msgid "<a id=\"read-only\"></a>\n"
msgstr "<a id=\"read-only\"></a>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Tails cannot use a read-only Persistent Storage. If your Tails USB stick has\n"
"a read-only switch, ensure it is in the read-write position.</p>\n"
msgstr ""
"<p>Tails no pot utilitzar un Emmagatzematge Persistent de només lectura. Si el vostre llapis USB de Tails té\n"
"un interruptor de només lectura, assegureu-vos que estigui en posició de lectura i escriptura.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"
