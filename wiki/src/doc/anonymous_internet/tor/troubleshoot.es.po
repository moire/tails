# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-14 22:38+0000\n"
"PO-Revision-Date: 2023-12-30 20:22+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Troubleshooting connecting to Tor\"]]\n"
msgstr "[[!meta title=\"Solución de problemas de conexión a Tor\"]]\n"

#. type: Plain text
msgid ""
"The sections below summarize the most common problems when [[connecting to "
"Tor|tor]]."
msgstr ""
"Las siguientes secciones resumen los problemas más comunes para [[conectarse "
"a la red Tor|tor]]."

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"clock\">The computer clock is set to an incorrect time zone</h2>\n"
msgstr "<h2 id=\"clock\">El reloj de la computadora está configurado en una zona horaria incorrecta</h2>\n"

#. type: Plain text
msgid "The clock of the computer needs to be correct to connect to Tor."
msgstr "El reloj de la computadora debe estar correcto para conectarse a Tor."

#. type: Plain text
msgid ""
"If you choose to [[hide that you are connecting to Tor|tor#hiding]], you "
"might have to fix the clock manually if Tails fails to connect to Tor."
msgstr ""

#. type: Plain text
msgid ""
"Choose **Fix Clock** in the error screen of the *Tor Connection* assistant "
"to set the clock and time zone of your computer."
msgstr ""
"Elige **Reparar reloj** en la pantalla de error del asistente *Tor "
"Connection* para configurar el reloj y la zona horaria de tu computadora."

#. type: Plain text
msgid ""
"If you have problems fixing the clock, you can instead try to restart Tails "
"and choose to [[connect to Tor automatically|tor#automatic]]. When "
"connecting to Tor automatically, Tails fixes the clock automatically by "
"connecting to the captive portal detection service of [Fedora](https://"
"getfedora.org/) before connecting to Tor."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"portal\">You need to sign in to the network</h2>\n"
msgstr "<h2 id=\"portal\">Necesitas iniciar sesión en la red</h2>\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/anonymous_internet/unsafe_browser/captive_portal.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/anonymous_internet/unsafe_browser/captive_portal.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
msgid "To sign in to a network using a captive portal:"
msgstr "Para iniciar sesión en una red usando un portal cautivo:"

#. type: Bullet: '1. '
msgid ""
"In the error screen of the *Tor Connection* assistant, choose **Try Signing "
"in to the Network**."
msgstr ""
"En la pantalla de error del asistente *Tor Connection*, elige **Intenta "
"iniciar sesión en la red**."

#. type: Bullet: '1. '
msgid "Wait until the *Unsafe Browser* starts."
msgstr "Espera hasta que se inicie el *Navegador No Seguro*."

#. type: Bullet: '1. '
msgid "Follow the instructions on the homepage of the *Unsafe Browser*."
msgstr ""
"Sigue las instrucciones en la página de inicio del *Navegador No Seguro*."

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"stuck\">The progress bar gets stuck around 50%</h2>\n"
msgstr "<h2 id=\"stuck\">La barra de progreso se atasca alrededor del 50%</h2>\n"

#. type: Plain text
msgid ""
"When using a custom Tor obfs4 bridge, the progress bar of *Tor Connection* "
"sometimes gets stuck halfway through and becomes extremely slow."
msgstr ""
"Cuando se utiliza un puente Tor obfs4 personalizado, la barra de progreso de "
"*Tor Connection* a veces se atasca a la mitad y se vuelve extremadamente "
"lenta."

#. type: Plain text
#, no-wrap
msgid "[[!img stuck.png link=\"no\" alt=\"\"]]\n"
msgstr "[[!img stuck.png link=\"no\" alt=\"\"]]\n"

#. type: Plain text
msgid "To fix this, you can either:"
msgstr "Para solucionar este problema, puedes:"

#. type: Plain text
msgid "- Close and reopen *Tor Connection* to speed up the initial connection."
msgstr ""
"- Cerrar y volver a abrir *Tor Connection* para acelerar la conexión inicial."

#. type: Plain text
msgid "- Try a different obfs4 bridge."
msgstr "- Prueba con un puente obfs4 diferente."

#. type: Plain text
#, no-wrap
msgid "<div class=\"bug\">\n"
msgstr "<div class=\"bug\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<!--\n"
"Last updated: 2023-05-04\n"
"-->\n"
msgstr ""
"<!--\n"
"Last updated: 2023-05-04\n"
"-->\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>This issue only affects outdated obfs4 bridges and does not happen with\n"
"obfs4 bridges that run version 0.0.12 or later.</p>\n"
msgstr ""
"<p>Este problema sólo afecta a los puentes obfs4 obsoletos y no ocurre con\n"
"los puentes obfs4 que ejecutan la versión 0.0.12 o posterior.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"broken\">The bridge is no longer operational</h2>\n"
msgstr "<h2 id=\"broken\">El puente ya no está operativo</h2>\n"

#. type: Plain text
msgid "It is possible that the bridge that you entered is no longer working."
msgstr "Es posible que el puente que ingresaste ya no funcione."

#. type: Plain text
msgid "Try entering another bridge or requesting other bridges."
msgstr "Intenta ingresar otro puente o solicitar otros puentes."

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"incorrect\">You did not enter the bridge correctly</h2>\n"
msgstr "<h2 id=\"incorrect\">No ingresaste el puente correctamente</h2>\n"

#. type: Plain text
msgid "Only obfs4 bridges can be used in Tails currently."
msgstr "Actualmente en Tails sólo se pueden usar puentes obfs4."

#. type: Plain text
msgid "An obfs4 bridge looks like:"
msgstr "Un puente obfs4 se parece a:"

#. type: Plain text
#, no-wrap
msgid "    obfs4 1.2.3.4:1234 B0E566C9031657EA7ED3FC9D248E8AC4F37635A4 cert=OYWq67L7MDApdJCctUAF7rX8LHvMxvIBPHOoAp0+YXzlQdsxhw6EapaMNwbbGICkpY8CPQ iat-mode=0\n"
msgstr "    obfs4 1.2.3.4:1234 B0E566C9031657EA7ED3FC9D248E8AC4F37635A4 cert=OYWq67L7MDApdJCctUAF7rX8LHvMxvIBPHOoAp0+YXzlQdsxhw6EapaMNwbbGICkpY8CPQ iat-mode=0\n"

#. type: Plain text
msgid ""
"You need to enter the entire line, not just the IP address and port "
"combination."
msgstr ""
"Debes ingresar toda la línea, no sólo la combinación de dirección IP y "
"puerto."
