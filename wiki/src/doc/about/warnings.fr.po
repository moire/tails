# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-04-11 14:09+0000\n"
"PO-Revision-Date: 2024-05-08 18:07+0000\n"
"Last-Translator: Chre <tor@renaudineau.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Content of: <div>
msgid "[[!meta title=\"Warnings: Tails is safe but not magic!\"]]"
msgstr "[[!meta title=\"Avertissements : Tails est sûr mais pas magique !\"]]"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta stylesheet=\"doc/about/warnings\" rel=\"stylesheet\" title=\"\"]]"
msgstr ""
"[[!meta stylesheet=\"doc/about/warnings\" rel=\"stylesheet\" title=\"\"]]"

#. type: Content of: <div><p>
msgid ""
"Tails is safer than any regular operating system. But Tails, or any software "
"or operating system, cannot protect you from everything—even if they pretend "
"to."
msgstr ""
"Tails est plus sûr que n'importe quel autre système d'exploitation "
"ordinaire. Mais Tails, ou tout autre logiciel ou système d'exploitation, ne "
"peut pas vous protéger de tout - même s'ils le prétendent."

#. type: Content of: <div><p>
msgid ""
"The recommendations below will keep you even safer, especially if you are at "
"high risk."
msgstr ""
"Les recommandations ci-dessous vous permettront d'être encore plus en "
"sécurité, surtout si vous courez un risque élevé."

#. type: Content of: <div><div><div><h3>
msgid "Protecting your identity when using Tails"
msgstr "Protéger votre identité lorsque vous utilisez Tails"

#. type: Content of: <div><div><div>
msgid ""
"[[!img identity.png link=\"no\" class=\"svg\"]] [[!inline pages=\"doc/about/"
"warnings/identity.inline\" raw=\"yes\" sort=\"age\"]]"
msgstr ""
"[[!img identity.png link=\"no\" class=\"svg\"]] [[!inline pages=\"doc/about/"
"warnings/identity.inline.fr\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div><div><div>
msgid "<button id=\"toggle-identity\">Protecting your identity</button>"
msgstr "<button id=\"toggle-identity\">Protéger votre identité</button>"

#. type: Content of: <div><div><div><h3>
msgid "Limitations of the Tor network"
msgstr "Limites du réseau Tor"

#. type: Content of: <div><div><div>
msgid ""
"[[!img tor.png link=\"no\" class=\"svg\"]] [[!inline pages=\"doc/about/"
"warnings/tor.inline\" raw=\"yes\" sort=\"age\"]]"
msgstr ""
"[[!img tor.png link=\"no\" class=\"svg\"]] [[!inline pages=\"doc/about/"
"warnings/tor.inline.fr\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div><div><div>
msgid "<button id=\"toggle-tor\">Limitations of Tor</button>"
msgstr "<button id=\"toggle-tor\">Limites de Tor</button>"

#. type: Content of: <div><div><div><h3>
msgid "Reducing risks when using untrusted computers"
msgstr "Réduire les risques lors de l'utilisation d'ordinateurs non fiables"

#. type: Content of: <div><div><div>
msgid ""
"[[!img computer.png link=\"no\" class=\"svg\"]] [[!inline pages=\"doc/about/"
"warnings/computer.inline\" raw=\"yes\" sort=\"age\"]]"
msgstr ""
"[[!img computer.png link=\"no\" class=\"svg\"]] [[!inline pages=\"doc/about/"
"warnings/computer.inline.fr\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div><div><div>
msgid "<button id=\"toggle-computer\">Using untrusted computers</button>"
msgstr ""
"<button id=\"toggle-computer\">Utiliser des ordinateurs non fiables</button>"

#. type: Content of: <div>
msgid ""
"<span id=\"hide-identity\" class=\"hide-button\"></span> [[!inline "
"pages=\"doc/about/warnings/identity\" raw=\"yes\" sort=\"age\"]]"
msgstr ""
"<span id=\"hide-identity\" class=\"hide-button\"></span> [[!inline "
"pages=\"doc/about/warnings/identity.fr\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div>
msgid ""
"<span id=\"hide-tor\" class=\"hide-button\"></span> [[!inline pages=\"doc/"
"about/warnings/tor\" raw=\"yes\" sort=\"age\"]]"
msgstr ""
"<span id=\"hide-tor\" class=\"hide-button\"></span> [[!inline pages=\"doc/"
"about/warnings/tor.fr\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div>
msgid ""
"<span id=\"hide-computer\" class=\"hide-button\"></span> [[!inline "
"pages=\"doc/about/warnings/computer\" raw=\"yes\" sort=\"age\"]]"
msgstr ""
"<span id=\"hide-computer\" class=\"hide-button\"></span> [[!inline "
"pages=\"doc/about/warnings/computer.fr\" raw=\"yes\" sort=\"age\"]]"

#. type: Content of: <div><p>
msgid ""
"Because you always have to adapt your digital security practices to your "
"specific needs and threats, we encourage you to learn more by reading the "
"following guides:"
msgstr ""
"Parce que vous devez toujours adapter vos pratiques de sécurité numérique à "
"vos besoins et aux menaces, nous vous encourageons à en apprendre plus en "
"lisant les guides suivants :"

#. type: Content of: <div><ul><li>
msgid "<a href=\"https://ssd.eff.org/\">EFF: Surveillance Self-Defense</a>"
msgstr "<a href=\"https://ssd.eff.org/fr\">EFF : Surveillance Self-Defense</a>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"https://securityinabox.org/\">Front Line Defenders: Security-in-a-"
"Box</a>"
msgstr ""
"<a href=\"https://securityinabox.org/fr\">Front Line Defenders : Security-in-"
"a-Box</a>"

#~ msgid ""
#~ "[[!toggleable id=\"identity\" text=\"\"\" <span class=\"hide\">[[!toggle "
#~ "id=\"identity\" text=\"\"]]</span> [[!inline pages=\"doc/about/warnings/"
#~ "identity\" raw=\"yes\" sort=\"age\"]] \"\"\"]] [[!toggleable id=\"tor\" "
#~ "text=\"\"\" <span class=\"hide\">[[!toggle id=\"tor\" text=\"\"]]</span> "
#~ "[[!inline pages=\"doc/about/warnings/tor\" raw=\"yes\" sort=\"age\"]] "
#~ "\"\"\"]] [[!toggleable id=\"computer\" text=\"\"\" <span "
#~ "class=\"hide\">[[!toggle id=\"computer\" text=\"\"]]</span> [[!inline "
#~ "pages=\"doc/about/warnings/computer\" raw=\"yes\" sort=\"age\"]] \"\"\"]]"
#~ msgstr ""
#~ "[[!toggleable id=\"identity\" text=\"\"\" <span class=\"hide\">[[!toggle "
#~ "id=\"identity\" text=\"\"]]</span> [[!inline pages=\"doc/about/warnings/"
#~ "identity.fr\" raw=\"yes\" sort=\"age\"]] \"\"\"]] [[!toggleable "
#~ "id=\"tor\" text=\"\"\" <span class=\"hide\">[[!toggle id=\"tor\" "
#~ "text=\"\"]]</span> [[!inline pages=\"doc/about/warnings/tor.fr\" "
#~ "raw=\"yes\" sort=\"age\"]] \"\"\"]] [[!toggleable id=\"computer\" "
#~ "text=\"\"\" <span class=\"hide\">[[!toggle id=\"computer\" text=\"\"]]</"
#~ "span> [[!inline pages=\"doc/about/warnings/computer.fr\" raw=\"yes\" "
#~ "sort=\"age\"]] \"\"\"]]"
