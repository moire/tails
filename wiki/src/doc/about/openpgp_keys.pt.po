# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-05-08 15:19+0000\n"
"PO-Revision-Date: 2024-05-10 20:53+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Portuguese <http://translate.tails.boum.org/projects/tails/"
"openpgp_keys/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"OpenPGP keys\"]]\n"
msgstr "[[!meta title=\"Chaves OpenPGP\"]]\n"

#. type: Plain text
msgid "Tails developers maintain several OpenPGP key pairs."
msgstr ""
"As pessoas que desenvolvem Tails mantêm diversos pares de chaves OpenPGP."

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Make sure to verify the keys that you download, because there are\n"
"several fake and maybe malicious Tails keys on the key servers.</p>\n"
msgstr ""
"<p>Certifique-se de verificar as chaves que você baixar, porque existem\n"
"várias chaves do Tails falsas, e talvez maliciosas, nos servidores de chaves.</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>For example, if you first [[authenticate the Tails signing key\n"
"through the OpenPGP Web of Trust|install/download#wot]], then\n"
"you can verify our others keys as they are all certified by the Tails\n"
"signing key.</p>\n"
msgstr ""
"<p>Por exemplo, se você primeiro [[autenticar a chave de assinatura do Tails\n"
"através da rede de confiança (Web of Trust) do OpenPGP|install/download#wot]],\n"
"poderá então verificar as nossas outras chaves, uma vez que todas são certificadas\n"
"pela chave de assinatura do Tails.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"private\"></a>\n"
msgstr "<a id=\"private\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Private mailing list key"
msgstr "Chave da lista privada de discussões"

#. type: Title -
#, no-wrap
msgid "Purpose"
msgstr "Propósito"

#. type: Title ###
#, no-wrap
msgid "Encryption"
msgstr "Criptografia"

#. type: Plain text
msgid ""
"This key has an encryption subkey. Please use it to send encrypted emails to "
"our contact address: [[info@tails.net|about/contact#tails]]."
msgstr ""
"Esta chave possui uma subchave de criptografia de dados. Por favor utilize-a "
"para enviar emails criptografados para nosso endereço de contato: "
"[[info@tails.net|about/contact#tails]]."

#. type: Title -
#, no-wrap
msgid "Policy"
msgstr "Política"

#. type: Plain text
msgid ""
"The secret key material and its passphrase are stored on the server that "
"runs our encrypted mailing list software and on systems managed by core "
"Tails developers."
msgstr ""
"O material secreto da chave e sua senha são armazenados no servidor onde o "
"software de listas de discussão criptografado roda e em sistemas gerenciados "
"por desenvolvedores/as do núcleo do Tails."

#. type: Title -
#, no-wrap
msgid "Key details"
msgstr "Detalhes da chave"

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          BDDC755D6B2D9FA09F0B92278C1487D924461904\n"
"    uid           [ unknown] info@tails.net <info@tails.net>\n"
"    uid           [ unknown] info@tails.net <info-request@tails.net>\n"
"    uid           [ unknown] info@tails.net <info-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"
msgstr ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          BDDC755D6B2D9FA09F0B92278C1487D924461904\n"
"    uid           [ unknown] info@tails.net <info@tails.net>\n"
"    uid           [ unknown] info@tails.net <info-request@tails.net>\n"
"    uid           [ unknown] info@tails.net <info-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"

#. type: Title -
#, no-wrap
msgid "How to get the public key?"
msgstr "Como obter essa chave?"

#. type: Plain text
msgid "There are multiple ways to get this OpenPGP public key:"
msgstr "Existem muitas formas de obter essa chave pública OpenPGP:"

#. type: Bullet: '- '
msgid "download it from this website: [[!tails_website info-tails-net.key]]"
msgstr "baixe-a deste website: [[!tails_website info-tails-net.key]]"

#. type: Bullet: '- '
msgid "fetch it from your favourite key server"
msgstr "baixe-a a partir do seu servidor de chaves preferido"

#. type: Bullet: '- '
msgid "send an email to <info-sendkey@tails.net>."
msgstr "envie um email para <info-sendkey@tails.net>."

#. type: Plain text
#, no-wrap
msgid "<a id=\"signing\"></a>\n"
msgstr "<a id=\"signing\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Signing key"
msgstr "Chave de assinatura"

#. type: Plain text
msgid ""
"This key only has the capability to sign and certify: it has no encryption "
"subkey."
msgstr ""
"Essa chave possui apenas a capacidade de assinar e certificar: ela não tem "
"uma subchave de criptografia."

#. type: Plain text
msgid "Its only purpose is:"
msgstr "Seus únicos propósitos são:"

#. type: Plain text
msgid "- To sign Tails released images"
msgstr "- Para assinar as imagens lançadas do Tails"

#. type: Bullet: '- '
msgid "To certify other cryptographic public keys needed for Tails development"
msgstr ""
"Certificar outras chaves públicas necessárias para o desenvolvimento do Tails"

#. type: Plain text
msgid ""
"The secret key material will never be stored on an online server or on "
"systems managed by anyone other than Tails core developers."
msgstr ""
"Os dados secretos da chave jamais serão armazenados em um servidor online ou "
"em sistemas gerenciados por quaisquer pessoas que não sejam desenvolvedoras "
"principais do Tails."

#. type: Title ###
#, no-wrap
msgid "Primary key"
msgstr "Chave primária"

#. type: Bullet: '- '
msgid ""
"Is not owned in a usable format by any single individual. It is split "
"cryptographically using [gfshare](https://tracker.debian.org/pkg/libgfshare)."
msgstr ""
"Não se encontra na posse de uma única pessoa em formato utilizável. É "
"dividida criptograficamente usando [gfshare](https://tracker.debian.org/pkg/"
"libgfshare)."

#. type: Bullet: '- '
msgid ""
"Is only used offline, in an air-gapped Tails only communicating with the "
"outside world through:"
msgstr ""
"Somente é usada de forma offline, em um Tails desconectado da Internet e de "
"outras redes que se comunica com o resto do mundo apenas através de:"

#. type: Bullet: '  * '
msgid ""
"Plugging the Tails flash media in another operating system to install Tails "
"in the first place."
msgstr ""
"Conexão da mídia que rodará o Tails em outro sistema operacional para fazer "
"uma instalação inicial do Tails."

#. type: Bullet: '  * '
msgid ""
"Plugging other removable media in the air-gapped Tails to send the public "
"key, secret key stubs, parts of the secret master key, and so on to the "
"outside world."
msgstr ""
"Conexão de outras mídias removíveis no Tails offline para enviar a chave "
"pública, fragmentos de chaves secretas, partes da chave secreta mestra, etc, "
"para o mundo externo."

#. type: Bullet: '  * '
msgid ""
"Plugging other removable media in the air-gapped Tails to receive Debian "
"packages, public keys, and so on from the outside world."
msgstr ""
"Conexão de outras mídias removíveis no Tails offline para receber pacotes "
"Debian, chaves públicas, etc, vindas do mundo externo."

#. type: Bullet: '- '
msgid ""
"Expires in less than one year. We will extend its validity as many times as "
"we find reasonable."
msgstr ""
"Expira em menos de um ano. Estenderemos sua validade periodicamente enquanto "
"considerarmos razoável."

#. type: Bullet: '- '
msgid ""
"Has a revocation certificate split amongst different people.  See the "
"[[details of the mechanism|signing_key_revocation]]."
msgstr ""
"Possui um certificado de revogação dividido em diversos pedaços distribuídos "
"para diferentes pessoas. Veja os [[detalhes deste mecanismo|"
"signing_key_revocation]]."

#. type: Title ###
#, no-wrap
msgid "Signing subkeys"
msgstr "Sub-chave de assinatura"

#. type: Bullet: '- '
msgid ""
"Stored on OpenPGP smartcards owned by those who need them.  Smartcards "
"ensure that the cryptographic operations are done on the smartcard itself "
"and that the secret cryptographic material is not directly available to the "
"operating system using it."
msgstr ""
"Armazenadas em smartcards que ficam em posse das pessoas que precisam delas. "
"Smartcards garantem que as operações criptográficas sejam realizadas dentro "
"dos próprios smartcards e que o material criptográfico secreto não fique "
"diretamente acessível ao sistema operacional que o está utilizando."

#. type: Plain text
msgid "- Expiration date: same as the primary key."
msgstr "- Data de expiração: a mesma da chave primária."

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096/0xDBB802B258ACD84F 2015-01-18 [C] [expires: 2025-01-25]\n"
"          Key fingerprint = A490 D0F4 D311 A415 3E2B  B7CA DBB8 02B2 58AC D84F\n"
"    uid                   [  full  ] Tails developers (offline long-term identity key) <tails@boum.org>\n"
"    uid                   [  full  ] Tails developers <tails@boum.org>\n"
"    sub   rsa4096/0xD21DAD38AF281C0B 2017-08-28 [S] [expires: 2025-01-25]\n"
"    sub   ed25519/0x90B2B4BD7AED235F 2017-08-28 [S] [expires: 2025-01-25]\n"
"    sub   rsa4096/0x7BFBD2B902EE13D0 2021-10-14 [S] [expires: 2025-01-25]\n"
"    sub   rsa4096/0xE5DBA2E186D5BAFC 2023-10-03 [S] [expires: 2025-01-25]\n"
msgstr ""
"    pub   rsa4096/0xDBB802B258ACD84F 2015-01-18 [C] [expires: 2025-01-25]\n"
"          Key fingerprint = A490 D0F4 D311 A415 3E2B  B7CA DBB8 02B2 58AC D84F\n"
"    uid                   [  full  ] Tails developers (offline long-term identity key) <tails@boum.org>\n"
"    uid                   [  full  ] Tails developers <tails@boum.org>\n"
"    sub   rsa4096/0xD21DAD38AF281C0B 2017-08-28 [S] [expires: 2025-01-25]\n"
"    sub   ed25519/0x90B2B4BD7AED235F 2017-08-28 [S] [expires: 2025-01-25]\n"
"    sub   rsa4096/0x7BFBD2B902EE13D0 2021-10-14 [S] [expires: 2025-01-25]\n"
"    sub   rsa4096/0xE5DBA2E186D5BAFC 2023-10-03 [S] [expires: 2025-01-25]\n"

#. type: Plain text
msgid ""
"To get this OpenPGP public key, download it from this website: [[!"
"tails_website tails-signing.key]]."
msgstr ""
"Para obter esta chave pública OpenPGP, baixe-a deste website: [[!"
"tails_website tails-signing.key]]."

#. type: Plain text
msgid ""
"If you already have Tails signing key but download it again, it can update "
"the list of existing signatures of the key."
msgstr ""
"Se você já possui a chave de assinatura do Tails mas baixou-a novamente, "
"pode ser que a lista de assinaturas na chave seja atualizada."

#. type: Plain text
#, no-wrap
msgid "<a id=\"support\"></a>\n"
msgstr "<a id=\"support\"></a>\n"

#. type: Title =
#, no-wrap
msgid "User support key"
msgstr "Chave para suporte a usuários"

#. type: Bullet: '- '
msgid ""
"Use this key to encrypt private support requests sent to [[tails-support-"
"private@boum.org|about/contact#tails-support-private]]."
msgstr ""
"Use esta chave para criptografar pedidos privados de suporte enviados para "
"[[tails-support-private@boum.org|about/contact#tails-support-private]]."

#. type: Plain text
msgid ""
"- This same key is used to handle [[*WhisperBack* reports|first_steps/"
"whisperback]]."
msgstr ""
"- Esta mesma chave é utilizada para gerir [[relatórios *WhisperBack*|"
"first_steps/whisperback]]."

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096 2013-07-24 [SC] [expires: 2024-01-06]\n"
"          1F56 EDD3 0741 0480 35DA  C1C5 EC57 B56E F0C4 3132\n"
"    uid           [  full  ] Tails bug squad <tails-bugs@boum.org>\n"
"    uid           [  full  ] Tails private user support <tails-support-private@boum.org>\n"
"    uid           [  full  ] Tails bug squad (schleuder list) <tails-bugs-owner@boum.org>\n"
"    uid           [  full  ] Tails bug squad (schleuder list) <tails-bugs-request@boum.org>\n"
"    sub   rsa4096 2013-07-24 [E] [expires: 2024-01-06]\n"
"          0012 C228 1573 FE8D 1C24  E350 9D6D 6472 AFC1 AD77\n"
msgstr ""
"    pub   rsa4096 2013-07-24 [SC] [expires: 2024-01-06]\n"
"          1F56 EDD3 0741 0480 35DA  C1C5 EC57 B56E F0C4 3132\n"
"    uid           [  full  ] Tails bug squad <tails-bugs@boum.org>\n"
"    uid           [  full  ] Tails private user support <tails-support-private@boum.org>\n"
"    uid           [  full  ] Tails bug squad (schleuder list) <tails-bugs-owner@boum.org>\n"
"    uid           [  full  ] Tails bug squad (schleuder list) <tails-bugs-request@boum.org>\n"
"    sub   rsa4096 2013-07-24 [E] [expires: 2024-01-06]\n"
"          0012 C228 1573 FE8D 1C24  E350 9D6D 6472 AFC1 AD77\n"

#. type: Plain text
msgid "- Download it from this website: [[!tails_website tails-bugs.key]]."
msgstr "- Baixe-a deste website: [[!tails_website tails-bugs.key]]."

#. type: Plain text
msgid "- Fetch it from your favourite key server."
msgstr "- Baixe-a a partir do seu servidor de chaves preferido."

#. type: Plain text
#, no-wrap
msgid "<a id=\"press\"></a>\n"
msgstr "<a id=\"press\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Press team key"
msgstr "Chave do Time de Comunicação"

#. type: Bullet: '- '
msgid ""
"Use this key to encrypt private emails sent to [[press@tails.net|about/"
"contact#tails-press]]."
msgstr ""
"Use esta chave para criptografar emails privados enviados para [[press@tails."
"net|about/contact#tails-press]]."

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          C54B2D6C229607035EECE4D83114691BD78DF1ED\n"
"    uid           [ unknown] press@tails.net <press@tails.net>\n"
"    uid           [ unknown] press@tails.net <press-request@tails.net>\n"
"    uid           [ unknown] press@tails.net <press-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"
msgstr ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          C54B2D6C229607035EECE4D83114691BD78DF1ED\n"
"    uid           [ unknown] press@tails.net <press@tails.net>\n"
"    uid           [ unknown] press@tails.net <press-request@tails.net>\n"
"    uid           [ unknown] press@tails.net <press-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"

#. type: Plain text
msgid ""
"- Download it from this website: [[!tails_website press-tails-net.key]]."
msgstr "- Baixe-a deste website: [[!tails_website press-tails-net.key]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"board\"></a>\n"
msgstr "<a id=\"board\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Board key"
msgstr "Chave do Conselho"

#. type: Plain text
msgid ""
"- Use this key to encrypt private emails sent to the [[Board|about/"
"contact#board]]."
msgstr ""
"- Use esta chave para criptografar emails privados enviados para o "
"[[Conselho|about/contact#board]]."

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          2DC4FED9D88C30D95A92675788E24FE2064F1511\n"
"    uid           [ unknown] board@tails.net <board@tails.net>\n"
"    uid           [ unknown] board@tails.net <board-request@tails.net>\n"
"    uid           [ unknown] board@tails.net <board-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"
msgstr ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          2DC4FED9D88C30D95A92675788E24FE2064F1511\n"
"    uid           [ unknown] board@tails.net <board@tails.net>\n"
"    uid           [ unknown] board@tails.net <board-request@tails.net>\n"
"    uid           [ unknown] board@tails.net <board-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"

#. type: Plain text
msgid ""
"- Download it from this website: [[!tails_website board-tails-net.key]]."
msgstr "- Baixe-a deste website: [[!tails_website board-tails-net.key]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"accounting\"></a>\n"
msgstr "<a id=\"accounting\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Accounting team key"
msgstr "Chave do time de Contabilidade"

#. type: Bullet: '- '
msgid ""
"Use this key to encrypt private emails sent to [[accounting@tails.net|about/"
"contact#tails-accounting]]."
msgstr ""
"Use esta chave para criptografar emails privados enviados para "
"[[accounting@tails.net|about/contact#tails-accounting]]."

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          EC58494A6333D0384DD5744DA38F0F6E9A0A234E\n"
"    uid           [ unknown] accounting@tails.net <accounting@tails.net>\n"
"    uid           [ unknown] accounting@tails.net <accounting-request@tails.net>\n"
"    uid           [ unknown] accounting@tails.net <accounting-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"
msgstr ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          EC58494A6333D0384DD5744DA38F0F6E9A0A234E\n"
"    uid           [ unknown] accounting@tails.net <accounting@tails.net>\n"
"    uid           [ unknown] accounting@tails.net <accounting-request@tails.net>\n"
"    uid           [ unknown] accounting@tails.net <accounting-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"

#. type: Plain text
msgid ""
"- Download it from this website: [[!tails_website accounting-tails-net.key]]."
msgstr "- Baixe-a deste website: [[!tails_website accounting-tails-net.key]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"fundraising\"></a>\n"
msgstr "<a id=\"fundraising\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Fundraising team key"
msgstr "Chave do time de Captação"

#. type: Bullet: '- '
msgid ""
"Use this key to encrypt private emails sent to [[fundraising@tails.net|about/"
"contact#tails-fundraising]]."
msgstr ""
"Use esta chave para criptografar emails privados enviados para "
"[[fundraising@tails.net|about/contact#tails-fundraising]]."

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          DA1C7908D1C0DC9EAA5915BC3A5B5685A641297B\n"
"    uid           [ unknown] fundraising@tails.net <fundraising@tails.net>\n"
"    uid           [ unknown] fundraising@tails.net <fundraising-request@tails.net>\n"
"    uid           [ unknown] fundraising@tails.net <fundraising-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"
msgstr ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          DA1C7908D1C0DC9EAA5915BC3A5B5685A641297B\n"
"    uid           [ unknown] fundraising@tails.net <fundraising@tails.net>\n"
"    uid           [ unknown] fundraising@tails.net <fundraising-request@tails"
".net>\n"
"    uid           [ unknown] fundraising@tails.net <fundraising-owner@tails."
"net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"

#. type: Plain text
msgid ""
"- Download it from this website: [[!tails_website fundraising-tails-net."
"key]]."
msgstr "- Baixe-a deste website: [[!tails_website fundraising-tails-net.key]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"foundations\"></a>\n"
msgstr "<a id=\"foundations\"></a>\n"

#. type: Plain text
#, no-wrap
msgid ""
"Foundations team key\n"
"==================\n"
msgstr ""
"Chave do time de Fundamentos\n"
"==================\n"

#. type: Bullet: '- '
msgid ""
"Use this key to encrypt private emails sent to [[foundations@tails.net|about/"
"contact#tails-foundations]]."
msgstr ""
"Use esta chave para criptografar emails privados enviados para "
"[[foundations@tails.net|about/contact#tails-foundations]]."

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          621609457A166C993245FFCA5F6B02630DDDE331\n"
"    uid           [ unknown] foundations@tails.net <foundations@tails.net>\n"
"    uid           [ unknown] foundations@tails.net <foundations-request@tails.net>\n"
"    uid           [ unknown] foundations@tails.net <foundations-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"
msgstr ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          621609457A166C993245FFCA5F6B02630DDDE331\n"
"    uid           [ unknown] foundations@tails.net <foundations@tails.net>\n"
"    uid           [ unknown] foundations@tails.net <foundations-request@tails.net>\n"
"    uid           [ unknown] foundations@tails.net <foundations-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"

#. type: Plain text
msgid ""
"- Download it from this website: [[!tails_website foundations-tails-net."
"key]]."
msgstr "- Baixe-a deste website: [[!tails_website foundations-tails-net.key]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"sysadmins\"></a>\n"
msgstr "<a id=\"sysadmins\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Sysadmins team key"
msgstr "Chave do time de Sysadmins"

#. type: Bullet: '- '
msgid ""
"Use this key to encrypt private emails sent to [[sysadmins@tails.net|about/"
"contact#tails-sysadmins]]."
msgstr ""
"Use esta chave para criprografar emails privados enviados para "
"[[sysadmins@tails.net|about/contact#tails-sysadmins]]."

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096 2024-04-26 [SC]\n"
"          0082D4D63B722D1FA27518A5C8F12D2B1AE1CB26\n"
"    uid           [ unknown] sysadmins@tails.net <sysadmins@tails.net>\n"
"    uid           [ unknown] sysadmins@tails.net <sysadmins-request@tails.net>\n"
"    uid           [ unknown] sysadmins@tails.net <sysadmins-owner@tails.net>\n"
"    sub   rsa4096 2024-04-26 [E]\n"
msgstr ""
"    pub   rsa4096 2024-04-26 [SC]\n"
"          0082D4D63B722D1FA27518A5C8F12D2B1AE1CB26\n"
"    uid           [ unknown] sysadmins@tails.net <sysadmins@tails.net>\n"
"    uid           [ unknown] sysadmins@tails.net <sysadmins-request@tails."
"net>\n"
"    uid           [ unknown] sysadmins@tails.net <sysadmins-owner@tails.net>"
"\n"
"    sub   rsa4096 2024-04-26 [E]\n"

#. type: Plain text
msgid ""
"- Download it from this website: [[!tails_website sysadmins-tails-net.key]]."
msgstr "- Baixe-a deste website: [[!tails_website sysadmins-tails-net.key]]."

#. type: Plain text
#, no-wrap
msgid "<a id=\"weblate\"></a>\n"
msgstr "<a id=\"weblate\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Translation platform admins team key"
msgstr "Chave do time de adminstradores da Plataforma de Tradução"

#. type: Bullet: '- '
msgid ""
"Use this key to encrypt private emails sent to [[the admins of the "
"translation platform at weblate@tails.net|about/contact#tails-weblate]]."
msgstr ""
"Use esta chave para criptografar emails privados enviados para [[os "
"administradores da plataforma de tradução em weblate@tails.net|about/contact"
"#tails-weblate]]."

#. type: Plain text
#, no-wrap
msgid ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          EFECA896F429B066B83E0266B702FE73C485A41D\n"
"    uid           [ unknown] weblate@tails.net <weblate@tails.net>\n"
"    uid           [ unknown] weblate@tails.net <weblate-request@tails.net>\n"
"    uid           [ unknown] weblate@tails.net <weblate-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"
msgstr ""
"    pub   rsa4096 2024-04-30 [SC]\n"
"          EFECA896F429B066B83E0266B702FE73C485A41D\n"
"    uid           [ unknown] weblate@tails.net <weblate@tails.net>\n"
"    uid           [ unknown] weblate@tails.net <weblate-request@tails.net>\n"
"    uid           [ unknown] weblate@tails.net <weblate-owner@tails.net>\n"
"    sub   rsa4096 2024-04-30 [E]\n"

#. type: Plain text
msgid ""
"- Download it from this website: [[!tails_website weblate-tails-net.key]]."
msgstr "- Baixe-a deste website: [[!tails_website weblate-tails-net.key]]."

#~ msgid ""
#~ "This means people other than Tails developers are in a position to use "
#~ "this secret key. Tails developers trust these people enough to rely on "
#~ "them for running our encrypted mailing list, but still: this key pair is "
#~ "managed in a less safe way than our signing key."
#~ msgstr ""
#~ "Isso significa que pessoas que não são desenvolvedoras do Tails estão "
#~ "numa posição que as permite utilizar essa chave secreta. Os/as "
#~ "desenvolvedores/as do Tails confiam nessa pessoas o suficiente para "
#~ "contar com elas para rodar nossa lista de discussão criptografada, mas "
#~ "ainda assim: esse par de chaves é gerenciado de forma menos segura do que "
#~ "nossa chave de assinatura."

#, no-wrap
#~ msgid ""
#~ "    pub   rsa4096 2009-08-14 [SC] [expires: 2023-03-03]\n"
#~ "          09F6 BC8F EEC9 D8EE 005D  BAA4 1D29 75ED F93E 735F\n"
#~ "    uid           [  full  ] Tails developers (Schleuder mailing-list) <tails@boum.org>\n"
#~ "    uid           [  full  ] Tails list (schleuder list) <tails-owner@boum.org>\n"
#~ "    uid           [  full  ] Tails list (schleuder list) <tails-request@boum.org>\n"
#~ "    sub   rsa4096 2009-08-14 [E] [expires: 2023-03-03]\n"
#~ "          C394 8FE7 B604 C611 4E29  4DDF D843 C2F5 E893 82EB\n"
#~ msgstr ""
#~ "    pub   rsa4096 2009-08-14 [SC] [expires: 2023-03-03]\n"
#~ "          09F6 BC8F EEC9 D8EE 005D  BAA4 1D29 75ED F93E 735F\n"
#~ "    uid           [  full  ] Tails developers (Schleuder mailing-list) <tails@boum.org>\n"
#~ "    uid           [  full  ] Tails list (schleuder list) <tails-owner@boum.org>\n"
#~ "    uid           [  full  ] Tails list (schleuder list) <tails-request@boum.org>\n"
#~ "    sub   rsa4096 2009-08-14 [E] [expires: 2023-03-03]\n"
#~ "          C394 8FE7 B604 C611 4E29  4DDF D843 C2F5 E893 82EB\n"

#, no-wrap
#~ msgid ""
#~ "    pub   rsa4096/0x457080B5A072CBE3 2014-07-11 [SCEA]\n"
#~ "          Key fingerprint = F3CD 9B7B 4BDF 9995 DA22  088E 4570 80B5 A072 CBE3\n"
#~ "    uid                   [  undef ] Tails press team (schleuder list) <tails-press@boum.org>\n"
#~ "    uid                   [  undef ] Tails press team (schleuder list) <tails-press-owner@boum.org>\n"
#~ "    uid                   [  undef ] Tails press team (schleuder list) <tails-press-request@boum.org>\n"
#~ "    sub   rsa4096/0x5748DE3BC338BFFC 2014-07-11 [SEA]\n"
#~ msgstr ""
#~ "    pub   rsa4096/0x457080B5A072CBE3 2014-07-11 [SCEA]\n"
#~ "          Key fingerprint = F3CD 9B7B 4BDF 9995 DA22  088E 4570 80B5 A072 CBE3\n"
#~ "    uid                   [  undef ] Tails press team (schleuder list) <tails-press@boum.org>\n"
#~ "    uid                   [  undef ] Tails press team (schleuder list) <tails-press-owner@boum.org>\n"
#~ "    uid                   [  undef ] Tails press team (schleuder list) <tails-press-request@boum.org>\n"
#~ "    sub   rsa4096/0x5748DE3BC338BFFC 2014-07-11 [SEA]\n"

#~ msgid "- Download it from this website: [[!tails_website tails-press.key]]."
#~ msgstr "- Baixe-a deste website: [[!tails_website tails-press.key]]."

#~ msgid ""
#~ "Use this key to encrypt private emails sent to [[tails-fundraising@boum."
#~ "org|about/contact#tails-fundraising]]."
#~ msgstr ""
#~ "Use esta chave para criptografar emails privados enviados para [[tails-"
#~ "fundraising@boum.org|about/contact#tails-fundraising]]."

#, no-wrap
#~ msgid ""
#~ "    pub   rsa4096/0xFEB0D5A18EACAF99 2014-08-09 [SCEA]\n"
#~ "          Key fingerprint = 3910 BD9D 690B A8C5 692F  93F8 FEB0 D5A1 8EAC AF99\n"
#~ "    uid                   [ unknown] Tails fundraising team (schleuder list) <tails-fundraising@boum.org>\n"
#~ "    uid                   [ unknown] Tails fundraising team (schleuder list) <tails-fundraising-owner@boum.org>\n"
#~ "    uid                   [ unknown] Tails fundraising team (schleuder list) <tails-fundraising-request@boum.org>\n"
#~ "    sub   rsa4096/0xEDC585308B7A9217 2014-08-09 [SEA]\n"
#~ msgstr ""
#~ "    pub   rsa4096/0xFEB0D5A18EACAF99 2014-08-09 [SCEA]\n"
#~ "          Key fingerprint = 3910 BD9D 690B A8C5 692F  93F8 FEB0 D5A1 8EAC AF99\n"
#~ "    uid                   [ unknown] Tails fundraising team (schleuder list) <tails-fundraising@boum.org>\n"
#~ "    uid                   [ unknown] Tails fundraising team (schleuder list) <tails-fundraising-owner@boum.org>\n"
#~ "    uid                   [ unknown] Tails fundraising team (schleuder list) <tails-fundraising-request@boum.org>\n"
#~ "    sub   rsa4096/0xEDC585308B7A9217 2014-08-09 [SEA]\n"

#~ msgid ""
#~ "- Download it from this website: [[!tails_website tails-fundraising.key]]."
#~ msgstr "- Baixe-a deste website: [[!tails_website tails-fundraising.key]]."

#, no-wrap
#~ msgid "<a id=\"mirrors\"></a>\n"
#~ msgstr "<a id=\"mirrors\"></a>\n"

#, no-wrap
#~ msgid ""
#~ "Mirrors team key\n"
#~ "===================\n"
#~ msgstr ""
#~ "Chave do time de Espelhos\n"
#~ "===================\n"

#~ msgid ""
#~ "Use this key to encrypt private emails sent to [[tails-mirrors@boum.org|"
#~ "about/contact#tails-mirrors]]."
#~ msgstr ""
#~ "Use esta chave para criptografar emails privados enviados para [[tails-"
#~ "mirrors@boum.org|about/contact#tails-mirrors]]."

#, no-wrap
#~ msgid ""
#~ "    pub   rsa4096/0xD2EDA621B572DD73 2016-04-29 [SCEA]\n"
#~ "          Key fingerprint = 0B08 8E31 D4F8 E59A 3D39  9137 D2ED A621 B572 DD73\n"
#~ "    uid                   [ unknown] Tails mirror pool managers (schleuder list) <tails-mirrors@boum.org>\n"
#~ "    uid                   [ unknown] Tails mirror pool managers (schleuder list) <tails-mirrors-request@boum.org>\n"
#~ "    uid                   [ unknown] Tails mirror pool managers (schleuder list) <tails-mirrors-owner@boum.org>\n"
#~ "    sub   rsa4096/0x3DCFC1EB1C62C73C 2016-04-29 [SEA]\n"
#~ msgstr ""
#~ "    pub   rsa4096/0xD2EDA621B572DD73 2016-04-29 [SCEA]\n"
#~ "          Key fingerprint = 0B08 8E31 D4F8 E59A 3D39  9137 D2ED A621 B572 DD73\n"
#~ "    uid                   [ unknown] Tails mirror pool managers (schleuder list) <tails-mirrors@boum.org>\n"
#~ "    uid                   [ unknown] Tails mirror pool managers (schleuder list) <tails-mirrors-request@boum.org>\n"
#~ "    uid                   [ unknown] Tails mirror pool managers (schleuder list) <tails-mirrors-owner@boum.org>\n"
#~ "    sub   rsa4096/0x3DCFC1EB1C62C73C 2016-04-29 [SEA]\n"

#~ msgid ""
#~ "- Download it from this website: [[!tails_website tails-mirrors.key]]."
#~ msgstr "- Baixe-a deste website: [[!tails_website tails-mirrors.key]]."

#, no-wrap
#~ msgid ""
#~ "    pub   rsa4096/0x70F4F03116525F43 2012-08-23 [SC] [expires: 2024-10-11]\n"
#~ "          D113CB6D5131D34BA5F0FE9E70F4F03116525F43\n"
#~ "    uid                              Tails system administrators <tails-sysadmins@boum.org>\n"
#~ "    uid                              Tails system administrators (schleuder list) <tails-sysadmins-request@boum.org>\n"
#~ "    uid                              Tails system administrators (schleuder list) <tails-sysadmins-owner@boum.org>\n"
#~ "    sub   rsa4096/0x58BA940CCA0A30B4 2012-08-23 [E] [expires: 2024-10-11]\n"
#~ msgstr ""
#~ "    pub   rsa4096/0x70F4F03116525F43 2012-08-23 [SC] [expires: 2024-10-11]\n"
#~ "          D113CB6D5131D34BA5F0FE9E70F4F03116525F43\n"
#~ "    uid                              Tails system administrators <tails-sysadmins@boum.org>\n"
#~ "    uid                              Tails system administrators (schleuder list) <tails-sysadmins-request@boum.org>\n"
#~ "    uid                              Tails system administrators (schleuder list) <tails-sysadmins-owner@boum.org>\n"
#~ "    sub   rsa4096/0x58BA940CCA0A30B4 2012-08-23 [E] [expires: 2024-10-11]\n"

#~ msgid ""
#~ "- Download it from this website: [[!tails_website tails-sysadmins.key]]."
#~ msgstr "- Baixe-a deste website: [[!tails_website tails-sysadmins.key]]."

#, no-wrap
#~ msgid ""
#~ "    pub   rsa4096/0x0190E73C38F13068 2020-10-02 [SC]\n"
#~ "          Key fingerprint = 6AA6 4D2B 7D77 AD46 6667  E7BD 0190 E73C 38F1 3068\n"
#~ "    uid                      tails-weblate@boum.org <tails-weblate@boum.org>\n"
#~ "    uid                      tails-weblate@boum.org <tails-weblate-request@boum.org>\n"
#~ "    uid                      tails-weblate@boum.org <tails-weblate-owner@boum.org>\n"
#~ "    sub   rsa4096/0x2F7EC378C628BE30 2020-10-02 [E]\n"
#~ msgstr ""
#~ "    pub   rsa4096/0x0190E73C38F13068 2020-10-02 [SC]\n"
#~ "          Key fingerprint = 6AA6 4D2B 7D77 AD46 6667  E7BD 0190 E73C 38F1 3068\n"
#~ "    uid                      tails-weblate@boum.org <tails-weblate@boum.org>\n"
#~ "    uid                      tails-weblate@boum.org <tails-weblate-request@boum.org>\n"
#~ "    uid                      tails-weblate@boum.org <tails-weblate-owner@boum.org>\n"
#~ "    sub   rsa4096/0x2F7EC378C628BE30 2020-10-02 [E]\n"

#~ msgid ""
#~ "- Download it from this website: [[!tails_website tails-weblate.key]]."
#~ msgstr "- Baixe-a deste website: [[!tails_website tails-weblate.key]]."

#~ msgid ""
#~ "Use this key to encrypt private emails sent to [[tails-foundations@boum."
#~ "org|about/contact#tails-foundations]]."
#~ msgstr ""
#~ "Use esta chave para criptografar emails privados enviados para [[tails-"
#~ "foundations@boum.org|about/contact#tails-foundations]]."

#, no-wrap
#~ msgid ""
#~ "    pub   rsa4096/0xA827FE0D677E522C 2019-02-24 [SC]\n"
#~ "          Key fingerprint = EFC9 4A11 CBF6 F00F 509C  EB0C A827 FE0D 677E 522C\n"
#~ "    uid                   [ unknown] tails-foundations@boum.org <tails-foundations@boum.org>\n"
#~ "    uid                   [ unknown] tails-foundations@boum.org <tails-foundations-request@boum.org>\n"
#~ "    uid                   [ unknown] tails-foundations@boum.org <tails-foundations-owner@boum.org>\n"
#~ "    sub   rsa4096/0x244F9D7C6DF90D6D 2019-02-24 [E]\n"
#~ msgstr ""
#~ "    pub   rsa4096/0xA827FE0D677E522C 2019-02-24 [SC]\n"
#~ "          Key fingerprint = EFC9 4A11 CBF6 F00F 509C  EB0C A827 FE0D 677E 522C\n"
#~ "    uid                   [ unknown] tails-foundations@boum.org <tails-foundations@boum.org>\n"
#~ "    uid                   [ unknown] tails-foundations@boum.org <tails-foundations-request@boum.org>\n"
#~ "    uid                   [ unknown] tails-foundations@boum.org <tails-foundations-owner@boum.org>\n"
#~ "    sub   rsa4096/0x244F9D7C6DF90D6D 2019-02-24 [E]\n"

#, fuzzy
#~| msgid ""
#~| "download it from this website: [[!tails_website tails-foundations.key]]"
#~ msgid ""
#~ "- Download it from this website: [[!tails_website tails-foundations.key]]."
#~ msgstr "baixe-a deste website: [[!tails_website tails-foundations.key]]"

#, fuzzy
#~| msgid "download it from this website: [[!tails_website tails-board.key]]"
#~ msgid "- Download it from this website: [[!tails_website tails-board.key]]."
#~ msgstr "baixe-a deste website: [[!tails_website tails-board.key]]"

#, no-wrap
#~ msgid ""
#~ "    pub   rsa4096/0xC436090F4BB47C6F 2014-07-11 [SCEA]\n"
#~ "          Key fingerprint = 256D EB90 7788 0CD6 8167  8528 C436 090F 4BB4 7C6F\n"
#~ "    uid                   [  undef ] Tails accounting team (schleuder list) <tails-accounting@boum.org>\n"
#~ "    uid                   [  undef ] Tails accounting team (schleuder list) <tails-accounting-owner@boum.org>\n"
#~ "    uid                   [  undef ] Tails accounting team (schleuder list) <tails-accounting-request@boum.org>\n"
#~ "    sub   rsa4096/0x289A5B45A9E89475 2014-07-11 [SEA]\n"
#~ msgstr ""
#~ "    pub   rsa4096/0xC436090F4BB47C6F 2014-07-11 [SCEA]\n"
#~ "          Key fingerprint = 256D EB90 7788 0CD6 8167  8528 C436 090F 4BB4 7C6F\n"
#~ "    uid                   [  undef ] Tails accounting team (schleuder list) <tails-accounting@boum.org>\n"
#~ "    uid                   [  undef ] Tails accounting team (schleuder list) <tails-accounting-owner@boum.org>\n"
#~ "    uid                   [  undef ] Tails accounting team (schleuder list) <tails-accounting-request@boum.org>\n"
#~ "    sub   rsa4096/0x289A5B45A9E89475 2014-07-11 [SEA]\n"

#~ msgid "<a id=\"translations\"></a>\n"
#~ msgstr "<a id=\"translations\"></a>\n"

#, fuzzy
#~ msgid ""
#~ "download it from this website: [[!tails_website tails-translations.key]]"
#~ msgstr "baixe-a deste website: [[!tails_website tails-signing.key]]"

#~ msgid "Signature"
#~ msgstr "Assinatura"

#~ msgid "Private mailing list key\n"
#~ msgstr "Chave da lista privada de discussões\n"

#~ msgid "Purpose\n"
#~ msgstr "Propósito\n"

#~ msgid "Policy\n"
#~ msgstr "Política\n"

#~ msgid "Key details\n"
#~ msgstr "Detalhes da chave\n"

#~ msgid "How to get the public key?\n"
#~ msgstr "Como obter essa chave?\n"

#~ msgid "Signing key\n"
#~ msgstr "Chave de assinatura\n"

#~ msgid ""
#~ "This key also has the capability to sign and certify. Until Tails 0.5 and "
#~ "0.6~rc3, released images were signed by this key. This purpose is now "
#~ "deprecated: further releases will be signed by a dedicated, safer signing "
#~ "key. As of 2010 October 7th, our mailing list key signature only means "
#~ "our mailing list software checked the signed content was originally "
#~ "OpenPGP-signed by a Tails core developer."
#~ msgstr ""
#~ "Esta chave também possui a capacidade de assinar e certificar. Até o "
#~ "Tails 0.5 e 0.6~rc3, as imagens lançadas eram assinadas com esta chave. "
#~ "Esta prática agora é obsoleta e altamente não recomendada: lançamentos "
#~ "futuros serão assinados com uma chave dedicada e mais segura. Até 7 de "
#~ "Outubro de 2010, a assinatura desta chave na nossa lista de discussão "
#~ "significava apenas que o software de lista de discussão havia verificado "
#~ "que o conteúdo dos emails havia sido originalmente assinado via OpenPGP "
#~ "por um/a desenvolvedor/a do núcleo do Tails."
