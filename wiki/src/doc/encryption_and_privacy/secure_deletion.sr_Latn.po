# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:26+0100\n"
"PO-Revision-Date: 2023-03-13 17:34+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sr_Latn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!meta title=\"Securely deleting files\"]]\n"
msgstr "[[!meta title=\"Dateien sicher löschen und Speicherplatz bereinigen\"]]\n"

#. type: Plain text
msgid ""
"Tails previously included tools to *wipe* files and available disk space. We "
"removed these tools from Tails 6.0 (February 2024), because we think that "
"they are not reliable enough on modern storage devices, such as USB sticks "
"and Solid-State Drives (SSDs)."
msgstr ""

#. type: Plain text
msgid ""
"To better protect from data recovery, use encrypted volumes, overwrite the "
"entire device, or disintegrate it physically."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "<h1 id=\"risks\">Limitations of file deletion</h1>\n"
msgstr "Warum sicheres Löschen verwenden?\n"

#. type: Plain text
msgid "See also [[!wikipedia Data_erasure desc=\"Wikipedia: Data Erasure\"]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"delete\">Deleting a file does not erase its content</h2>\n"
msgstr ""

#. type: Plain text
msgid ""
"Operating systems do not actually remove the content of a file when the file "
"is deleted, even after emptying the trash or removing the file from the "
"command line."
msgstr ""

#. type: Plain text
msgid ""
"Instead, operating systems only mark the space that was used by the deleted "
"file as available for future files, because it is much faster to do. "
"However, the content of the deleted file remains on the device until the "
"operating system reuses the space for another file."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "<div class=\"attack\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid "<p>Simple data recovery tools can recover deleted files.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"copy\">Other copies of the data might be stored elsewhere</h2>\n"
msgstr ""

#. type: Plain text
msgid "Other copies of the data might exist elsewhere on the device:"
msgstr ""

#. type: Bullet: '- '
msgid ""
"Some applications, for example, office applications, store temporary backup "
"versions of the file to be able to recover from failures."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Modern operating systems can store a *journal*, a history of recent changes "
"to the file system, or *snapshots*, an image of the file system at a "
"particular point in time."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"spare\">Spare memory cells can store a hidden copy of the data</h2>\n"
msgstr ""

#. type: Plain text
msgid ""
"Flash memory devices, such as USB sticks and SSDs (Solid-State Drives), have "
"*spare memory cells* that are used to replace broken memory cells over "
"time.  Modern hard disks also integrate small flash memories for performance "
"and reliability."
msgstr ""

#. type: Plain text
msgid ""
"Any data that was stored in the device can remain available in these spare "
"cells."
msgstr ""

#. type: Plain text
msgid ""
"These spare cells are managed directly by the device and neither the user "
"nor the operating system can control what is stored in them."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>Specialized data recovery companies can recover data from spare cells.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"mitigation\">Mitigation techniques</h1>\n"
msgstr ""

#. type: Plain text
msgid ""
"Given the limitations described above, there is no perfect solution to file "
"deletion."
msgstr ""

#. type: Plain text
msgid ""
"To better protect from data recovery, you can use one or a combination of "
"these mitigation techniques, depending on the sensitivity of your data and "
"the risks."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "<h2 id=\"tails\">Use Tails to avoid saving files</h2>\n"
msgstr "Dateien sicher löschen\n"

#. type: Plain text
msgid ""
"All the files that you use in Tails disappear automatically when you shut "
"down, except the files that you choose to store in the Persistent Storage or "
"other storage devices."
msgstr ""

#. type: Plain text
msgid ""
"By making it easy to avoid saving files to storage devices in the first "
"place, using Tails is already an important mitigation technique given the "
"limitations of file deletion."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "<h2 id=\"encrypt\">Encrypt your storage devices</h2>\n"
msgstr "Sicheres Löschen von verfügbarem Speicherplatz\n"

#. type: Plain text
msgid ""
"When all the data on your device is encrypted, data recovery tools can only "
"recover encrypted data that is useless without your passphrase."
msgstr ""

#. type: Plain text
msgid ""
"To better protect deleted files from data recovery, you can reformat your "
"device and create another encryption with a different passphrase."
msgstr ""

#. type: Bullet: '- '
msgid ""
"For compatibility with Tails only or with another Linux, use [[LUKS "
"encrypted volumes|encrypted_volumes]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"For compatibility with Windows or macOS, use [[VeraCrypt encrypted volumes|"
"veracrypt]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>Take into account that you could be forced or tricked to give out your\n"
"passphrase.</p>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "<h2 id=\"overwrite\">Overwrite the entire device</h2>\n"
msgstr "Sicheres Löschen von verfügbarem Speicherplatz\n"

#. type: Plain text
msgid ""
"To better protect deleted files from data recovery, you can also reformat "
"and overwrite the entire device with zeroes."
msgstr ""

#. type: Plain text
msgid ""
"Even if some data could still be recovered from [[spare memory cells|"
"secure_deletion#spare]], overwriting the entire device is especially "
"important if the device is not encrypted."
msgstr ""

#. type: Plain text
msgid "To do so:"
msgstr ""

#. type: Bullet: '1. '
msgid "Open the *Disks* utility."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   All the current storage devices are listed in the left pane.\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Plug in the device that you want to reformat and overwrite."
msgstr ""

#. type: Bullet: '1. '
msgid "A new device appears in the list of devices. Click on it."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img encrypted_volumes/storage_devices_after.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Check that the brand and size of the device in the right pane corresponds to "
"your device."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Click on the [[!img lib/view-more.png alt=\"Drive Options\" "
"class=\"symbolic\" link=\"no\"]] button in the title bar and choose **Format "
"Disk**."
msgstr ""

#. type: Bullet: '1. '
msgid "In the **Format Disk** dialog:"
msgstr ""

#. type: Bullet: '   - '
msgid ""
"Choose **Overwrite existing data with zeroes (Slow)** in the **Erase** menu."
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "     <div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"     <p>Overwriting existing data does not erase all data on flash\n"
"     memories, such as USB sticks and SSDs (Solid-State Drives).</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "     <p>See the [[limitations of file deletion|doc/encryption_and_privacy/secure_deletion#spare]].</p>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "     </div>\n"
msgstr "</div>\n"

#. type: Bullet: '   - '
msgid ""
"Choose **Compatible with all systems and devices (MBR/DOS)** in the "
"**Partitioning** menu."
msgstr ""

#. type: Bullet: '1. '
msgid "Click **Format**."
msgstr ""

#. type: Bullet: '1. '
msgid "In the confirmation dialog, make sure that the device is correct."
msgstr ""

#. type: Bullet: '1. '
msgid "Click **Format** to confirm."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h2 id=\"destroy\">Physically destroy the device</h2>\n"
msgstr ""

#. type: Plain text
msgid ""
"As a last resort, physically destroying the device gives the strongest "
"guarantee against data recovery."
msgstr ""

#. type: Plain text
msgid ""
"It is unreliable to break the device into a few pieces or burn it with wood "
"fire."
msgstr ""

#. type: Plain text
msgid ""
"For top secret documents, the National Security Agency ([[!wikipedia NSA]]) "
"of the USA recommends disintegration into particles of 2 mm. This can be "
"achieved using a [good quality household blender](https://commons.erau.edu/"
"jdfsl/vol16/iss2/1/)."
msgstr ""

#, fuzzy, no-wrap
#~ msgid "<h1 id=\"disk_space\">Securely cleaning available disk space</h1>\n"
#~ msgstr "Sicheres Löschen von verfügbarem Speicherplatz\n"

#~ msgid "Warning about USB sticks and solid-state disks\n"
#~ msgstr "Warnung bezüglich USB-Sticks und Solid-State-Disks\n"

#~ msgid ""
#~ "**The methods described below will not work as expected on USB sticks "
#~ "and\n"
#~ "solid-state disks.**\n"
#~ msgstr ""
#~ "**Die unten beschriebenen Vorgehensweisen werden auf USB-Sticks\n"
#~ "und Solid-State-Disks nicht wie erwartet funktionieren**\n"

#~ msgid ""
#~ "The existing hard disk-oriented techniques for secure deletion of "
#~ "individual files are not effective."
#~ msgstr ""
#~ "Die bekannten, an Festplatten orientierten Vorgehensweisen für sicheres "
#~ "Löschen einzelner Dateien funktionieren nicht."

#~ msgid ""
#~ "Overwriting twice the entire disk is usually, but not always, sufficient "
#~ "to securely clean the disk."
#~ msgstr ""
#~ "Das gesamte Laufwerk zweimal zu überschreiben ist meistens, aber nicht "
#~ "immer, ausreichend, um das Laufwerk sicher zu bereinigen."

#~ msgid ""
#~ "<p>Unfortunately, Tails does not currently allow you to perform this "
#~ "task\n"
#~ "with graphical tools. See [[!tails_ticket 5323]].</p>\n"
#~ msgstr ""
#~ "<p>Leider ist es derzeit in Tails unmöglich, diese Aufgabe\n"
#~ "mit grafischen Werkzeugen zu erledigen. Lesen Sie hierzu [[!tails_ticket "
#~ "5323]].</p>\n"

#~ msgid ""
#~ "In Tails you can securely delete files thanks to an [extension of the "
#~ "file browser](http://wipetools.tuxfamily.org/nautilus-wipe.html)."
#~ msgstr ""
#~ "In Tails können Sie Dateien mit einer [Erweiterung des Dateimanagers]"
#~ "(http://wipetools.tuxfamily.org/nautilus-wipe.html) sicher löschen."

#~ msgid ""
#~ "  1. Open the file browser, either\n"
#~ "  from the <span class=\"guimenu\">Places</span> menu or the <span\n"
#~ "  class=\"guilabel\">Home</span> icon on the desktop.\n"
#~ msgstr ""
#~ "  1. Öffnen Sie den Dateimanager, entweder\n"
#~ "  vom <span class=\"guimenu\">Orte</span>-Menü oder dem <span\n"
#~ "  class=\"guilabel\">Persönlicher Ordner</span>-Symbol auf dem Desktop.\n"

#, fuzzy
#~ msgid ""
#~ "  1. Open the file browser, either from the <span "
#~ "class=\"guimenu\">Places</span> menu or\n"
#~ "  the <span class=\"guilabel\">Home</span> icon on the desktop.\n"
#~ msgstr ""
#~ "  1. Öffnen Sie den Dateimanager, entweder von dem <span "
#~ "class=\"guimenu\">Orte</span>-Menü oder\n"
#~ "  dem <span class=\"guilabel\">home</span>-Symbol auf dem Desktop.\n"

#~ msgid ""
#~ "     <div class=\"tip\">\n"
#~ "     <p>On the previous screenshot, the trash in the <span\n"
#~ "     class=\"filename\">.Trash-1000</span> folder is not deleted. See "
#~ "the\n"
#~ "     [[instructions above|secure_deletion#empty_trash]].</p>\n"
#~ "     </div>\n"
#~ msgstr ""
#~ "     <div class=\"tip\">\n"
#~ "     <p>Auf dem vorherigen Bildschirmfoto wird der Papierkorb in dem "
#~ "Ordner<span\n"
#~ "     class=\"filename\">.Trash-1000</span> nicht gelöscht. Lesen Sie die\n"
#~ "     [[obigen Anweisungen|secure_deletion#empty_trash]].</p>\n"
#~ "     </div>\n"

#~ msgid ""
#~ "<p>This option does not delete hidden files. Choose\n"
#~ "   <span class=\"menuchoice\">\n"
#~ "      <span class=\"guimenu\">[[!img lib/open-menu.png alt=\"Menu\" "
#~ "class=symbolic link=no]]</span>&nbsp;▸\n"
#~ "      <span class=\"guimenuitem\">Show hidden files</span></span>\n"
#~ "   in the titlebar to show them.\n"
#~ "</p>\n"
#~ msgstr ""
#~ "<p>Diese Option löscht keine verborgenen Dateien. Wählen Sie in der "
#~ "Titelleiste\n"
#~ "   <span class=\"menuchoice\">\n"
#~ "      <span class=\"guimenu\">[[!img lib/open-menu.png alt=\"Menu\" "
#~ "class=symbolic link=no]]</span>&nbsp;▸\n"
#~ "      <span class=\"guimenuitem\">Verborgene Dateien anzeigen</span></"
#~ "span>\n"
#~ "   aus, um diese anzuzeigen.\n"
#~ "</p>\n"
