# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2024-01-31 15:45+0100\n"
"PO-Revision-Date: 2023-11-16 09:14+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Most files contain metadata, which is information used to describe,\n"
"identify, categorize, and sort files. For example:</p>\n"
msgstr ""
"<p>Die meisten Dateien enthalten Metadaten, d. h. Informationen, die zur Beschreibung,\n"
"Identifizierung, Kategorisierung und Sortierung von Dateien dienen. Zum Beispiel:</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"<ul>\n"
"<li>Cameras record data about when and where a picture was taken and what\n"
"camera was used.</li>\n"
"<li>Office documents automatically add author\n"
"and company information to texts and spreadsheets.</li>\n"
"</ul>\n"
msgstr ""
"<ul>\n"
"<li>Kameras zeichnen Daten darüber auf, wann und wo ein Foto gemacht wurde und welche\n"
"Kamera benutzt wurde.</li>\n"
"<li>Dokumente fügen automatisch Autoren- und\n"
"Firmeninformationen zu Texten und Tabellenkalkulationen hinzu.</li>\n"
"</ul>\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<p>Use [[<i>mat2</i>|doc/sensitive_documents/metadata]] to\n"
#| "clean metadata from your files before publishing or sharing them:</p>\n"
msgid ""
"<p>Use [[<i>Metadata Cleaner</i>|doc/sensitive_documents/metadata]] to clean\n"
"metadata from your files before sharing them.</p>\n"
msgstr ""
"<p>Verwenden Sie [[<i>mat2</i>|doc/sensitive_documents/metadata]],\n"
"um Metadaten aus Ihren Dateien zu entfernen, bevor Sie diese veröffentlichen oder teilen:</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"
