# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-07-28 08:15+0000\n"
"PO-Revision-Date: 2024-05-12 14:26+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sr_Latn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 5.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Work on sensitive documents\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"[[!inline pages=\"doc/sensitive_documents.index\" raw=\"yes\" "
"sort=\"age\"]]\n"
msgstr ""
"[[!inline pages=\"doc/sensitive_documents.index.sr_Latn\" raw=\"yes\" sort="
"\"age\"]]\n"
