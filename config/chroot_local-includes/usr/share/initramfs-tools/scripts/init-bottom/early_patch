#!/bin/sh

set -x

PREREQS=""

prereqs() { echo "$PREREQS"; }

case "$1" in
	prereqs)
	prereqs
	exit 0
	;;
esac

set -eu

option="$(tr ' ' '\n' < /proc/cmdline | grep '^early_patch\b')"

[ -n "$option" ] || exit 0

. /scripts/functions

MOUNTPOINT=/root/mnt

run() {
  for mod in fscache 9pnet 9pnet_virtio 9p; do
    modprobe -vvv -d /root/ $mod
  done

  mkdir -p "$MOUNTPOINT"
  mount -t 9p src "$MOUNTPOINT"
  "${MOUNTPOINT}/hook" /root

  # early_patch=umount will umount the filesystem immediately after running the hook
  # this is needed to have early_patch in the test suite.
  # In fact, qemu cannot do snapshots while this is mounted
  if echo "$option" | grep -qw umount; then
      umount "$MOUNTPOINT"
  fi
}

spawn_shell() {
  # Make plymouth write the log to /root/var/log/boot.log
  /bin/plymouth update-root-fs --new-root-dir=/root --read-write
  # Stop plymouth, so that the user can see the shell
  /bin/plymouth quit || true

  # Disable xtrace to make the following output more readable
  set +x
  echo -e >&2 "\nThe early_patch hook failed. Spawning a shell to allow debugging..."
  echo >&2 "The log is in /root/var/log/boot.log"
  echo >&2 "Press Ctrl-D to continue booting."

  # Call panic from /usr/share/initramfs-tools/scripts/functions to
  # spawn a shell.
  . /scripts/functions
  panic
}

# This is necessary to have `set -e` work in the `run` function, see
# https://stackoverflow.com/a/11092989/2804197
set +e
(set -e; run); ret=$?

if [ "$ret" -ne 0 ]; then
  spawn_shell
fi
